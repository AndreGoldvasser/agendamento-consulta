<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="includes/menu.jsp" %>

 <div class="container">
  <h3>Lista de Paciente</h3> 
</div>

	<div class="container">
 		
			<table class="table">	
			<form action="PacienteControllerServlet" method="GET">										
			</form>
			
			<table class="table">
			
				<tr>
					<th>C�digo</th>
					<th>Nome</th>
					<th>CPF</th>
					<th>Cidade</th>
					<th>E-Mail</th>
					<th>Telefone</th>
					<th>Nascimento</th>	
					<th>Sexo</th>
					<th>Cart�o Sus</th>													
					<th>A��o</th>
				</tr>
				
				<c:forEach var="tempPaciente" items="${PACIENTE_LIST}">
					
					<c:url var="tempLink" value="PacienteControllerServlet">
						<c:param name="command" value="LOAD" />
						<c:param name="pacienteId" value="${tempPaciente.codPaciente}" />
					</c:url>

					<c:url var="deleteLink" value="PacienteControllerServlet">
						<c:param name="command" value="DELETE" />
						<c:param name="pacienteId" value="${tempPaciente.codPaciente}" />
					</c:url>
																
					<tr>
						<td> ${tempPaciente.codPaciente} </td>
						<td> ${tempPaciente.nomePaciente} </td>
						<td> ${tempPaciente.cpfPaciente} </td>
						<td>
						<c:forEach items="${CIDADES_FORM}" var="tempCid" >
							<c:if test="${tempPaciente.cidadePaciente == tempCid.codCidade }">
								${tempCid.nomeCidade}
							</c:if>
						</c:forEach>
						</td>					
						
						<td> ${tempPaciente.emailPaciente} </td>
						<td> ${tempPaciente.telefonePaciente} </td>
						<td> ${tempPaciente.datanascPaciente} </td>	
						<td> ${tempPaciente.sexoPaciente} </td>	
						<td> ${tempPaciente.cartaoPaciente} </td>						
						
						<td> 
							<a class="btn btn-warning" href="${tempLink}">Atualizar</a> 							 
							<a class="btn btn-danger" href="${deleteLink}"
							onclick="if (!(confirm('Tem certeza que deseja excluir registro?'))) return false">
							Apagar</a>	
						</td>
					</tr>
				
				</c:forEach>
				
			</table>
		
		</div>
	
	</div>
</body>
</html>









<%@include file="includes/menu.jsp" %>

 <div class="container">
  <h3>Atualizar Medico</h3> 
</div>
 	
	<div class="container">
		
		<form action="MedicoControllerServlet" method="GET">
		
			<input type="hidden" name="command" value="UPDATE" />

			<input type="hidden" name="medicoId" value="${MEDICO.codMedico}" />
			
			<table class="table">
				<tbody>			
					<tr>
						<td><label>Nome:</label></td>
						<td><input type="text" name="nomeMedico" maxlength="50"
								   value="${MEDICO.nomeMedico}" /></td>
					</tr>				
					
					<tr>
						<td><label>CRM:</label></td>
						<td><input type="number" name="crmMedico" maxlength="20"
								   value="${MEDICO.crmMedico}" /></td>
					</tr>
					
					<tr>
						<td><label>Especialidade:</label></td>
						<td>
						<select name="especialidadeMedico">
    						<c:forEach items="${ESPECIALIDADES_FORM}" var="tempEsp">
        						<option value="${tempEsp.codEspecialidade}"><c:out value="${tempEsp.nomeEspecialidade}" /></option>
    						</c:forEach>
						</select>
						</td>
					</tr>
					
						<tr>
						<td><label>RG:</label></td>
						<td><input type="number" name="rgMedico" maxlength="11" 
								   value="${MEDICO.rgMedico}" /></td>
					</tr>	
					
						<tr>
						<td><label>CPF:</label></td>
						<td><input type="number" name="cpfMedico" maxlength="11"
								   value="${MEDICO.cpfMedico}" /></td>
					</tr>	
					
						<tr>
						<td><label>Endere�o:</label></td>
						<td><input type="text" name="enderecoMedico" maxlength="50"
								   value="${MEDICO.enderecoMedico}" /></td>
					</tr>	
					
					<tr>
						<td><label>Cidade:</label></td>
						<td>
						<select name="cidadeMedico" >
    						<c:forEach items="${CIDADES_FORM}" var="tempCid">
        						<option value="${tempCid.codCidade}"><c:out value="${tempCid.nomeCidade}" /></option>
    						</c:forEach>
						</select>
						</td>
					</tr>
					
						<tr>
						<td><label>Bairro:</label></td>
						<td><input type="text" name="bairroMedico" maxlength="20"
								   value="${MEDICO.bairroMedico}" /></td>
					</tr>	
					
						<tr>
						<td><label>Estado:</label></td>
						<td><input type="text" name="estadoMedico" maxlength="2"
								   value="${MEDICO.estadoMedico}" /></td>
					</tr>	
					
					<tr>
						<td><label>Telefone:</label></td>
						<td><input type="tel" name="telefoneMedico" maxlength="20"
								   value="${MEDICO.telefoneMedico}" /></td>
					</tr>
					
					<tr>
						<td><label>E-mail:</label></td>
						<td><input type="email" name="emailMedico" max="50"
								   value="${MEDICO.emailMedico}" /></td>
					</tr>
					
					<tr>
						<td><label>Nascimento:</label></td>
						<td><input type="date" name="datanascMedico" 
								   value="${MEDICO.datanascMedico}" /></td>
					</tr>
					
					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Salvar" class="save" /></td>
					</tr>
					
				</tbody>
			</table>
		</form>
		
		<div style="clear: both;"></div>
		
		<p>
			<a href="MedicoControllerServlet">Voltar � Lista</a>
		</p>
	</div>
</body>

</html>












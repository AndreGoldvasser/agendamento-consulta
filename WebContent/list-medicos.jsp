<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="includes/menu.jsp" %>

 <div class="container">
  <h3>Lista de M�dicos</h3> 
</div>

	<div class="container">
 		
			<table class="table">	
			<form action="MedicoControllerServlet" method="GET">
			</form>
			<table class="table">
	
				<tr>
					<th>C�digo</th>
					<th>CRM</th>
					<th>Nome</th>
					<th>Especialidade</th>
					<th>Telefone</th>													
					<th>A��o</th>
				</tr>
				
				<c:forEach var="tempMedico" items="${MEDICO_LIST}">
					
					<c:url var="tempLink" value="MedicoControllerServlet">
						<c:param name="command" value="LOAD" />
						<c:param name="medicoId" value="${tempMedico.codMedico}" />
					</c:url>

					<c:url var="deleteLink" value="MedicoControllerServlet">
						<c:param name="command" value="DELETE" />
						<c:param name="medicoId" value="${tempMedico.codMedico}" />
					</c:url>
																	
					<tr>
						<td> ${tempMedico.codMedico} </td>
						<td> ${tempMedico.crmMedico} </td>
						<td> ${tempMedico.nomeMedico} </td>
						<td>
						<c:forEach items="${ESPECIALIDADES_FORM}" var="tempEspec" >
							<c:if test="${tempMedico.especialidadeMedico == tempEspec.codEspecialidade }">
								${tempEspec.nomeEspecialidade}
							</c:if>
						</c:forEach>
						</td>
									
						<td> ${tempMedico.telefoneMedico} </td>				
						
						<td> 
							<a class="btn btn-warning" href="${tempLink}">Atualizar</a> 								  
							<a class="btn btn-danger" href="${deleteLink}"
							onclick="if (!(confirm('Tem certeza que deseja excluir registro?'))) return false">
							Apagar</a>	
						</td>
					</tr>
				
				</c:forEach>
				
			</table>
		
		</div>
	
	</div>
</body>


</html>









<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="includes/menu.jsp"%>

<div class="container">
	<h3>Cadastro de Paciente</h3>

	<%-- Script que formata os campos corretamente --%>
	<%-- Requer jquery.mask(.min).js --%>
	<script type="text/javascript">
		$(document).ready(function(){
			
			$("#rg").mask("999.999.999-W", {
				translation: {
					'W': {
						pattern: /[X0-9]/
					}
				},
				reverse: true
			})
			
			$("#cpf").mask("000.000.000-00")
			$("#telefone").mask("(00) 0000-00009")
			$("#telefone").blur(function(event){
				if ($(this).val().length == 15){
					$("#telefone").mask("(00) 00000-0009")	
				}else{
					$("#telefone").mask("(00) 0000-0009")
				}
			})				
		})
		</script>





	<form id="formCadastroPaciente" action="PacienteControllerServlet"
		method="GET">
		<input type="hidden" name="command" value="ADD" />
		<table class="table">
			<tr>
				<td><label>Nome:</label></td>
				<td><input type="text" id="nome" name="nomePaciente"
					placeholder="John Doe" maxlength="50" required /></td>
			</tr>

			<tr>
				<td><label>RG:</label></td>
				<td><input type="text" id="rg" name="rgPaciente"
					placeholder="12345678900" maxlength="11" required /></td>
			</tr>

			<tr>
				<td><label>CPF:</label></td>
				<td><input type="text" id="cpf" name="cpfPaciente"
					placeholder="00987654321" maxlength="19" required /></td>
			</tr>
			<tr>
				<td><label>Endere�o:</label></td>
				<td><input type="text" name="enderecoPaciente"
					placeholder="Rua Xyz, 000" maxlength="50" required /></td>
			</tr>

			<tr>
				<td><label>Cidade:</label></td>
				<td><select name="cidadePaciente">
						<option value="">---SELECIONE---</option>
						<c:forEach items="${CIDADES_FORM}" var="tempCid">
							<option value="${tempCid.codCidade}"><c:out
									value="${tempCid.nomeCidade}" /></option>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td><label>Bairro:</label></td>
				<td><input type="text" name="bairroPaciente"
					placeholder="Meu Bairro" maxlength="20" required /></td>
			</tr>
			<tr>
				<td><label>Estado:</label></td>
				<td><input type="text" name="estadoPaciente" placeholder="XX"
					maxlength="2" required /></td>
			</tr>
			<tr>
				<td><label>Telefone:</label></td>
				<td><input type="tel" id="telefone" name="telefonePaciente"
					placeholder="2345678" maxlength="20" required /></td>
			</tr>
			<tr>
				<td><label>E-Mail:</label></td>
				<td><input type="email" id="email" name="emailPaciente"
					placeholder="email@email.com" maxlength="50" required /></td>
			</tr>

			<tr>
				<td><label>Data Nascimento:</label></td>
				<td><input type="date" id="datanasc" name="datanascPaciente"
					placeholder="00/00/0000" required /></td>
			</tr>

			<tr>
				<td><label>Sexo:</label></td>
				<td><input type="text" name="sexoPaciente" placeholder="M/F"
					required /></td>
			</tr>

			<tr>
				<td><label>Cart�o Sus:</label></td>
				<td><input type="text" name="cartaoPaciente"
					placeholder="123456789" required /></td>
			</tr>

			<tr>
				<td><label></label></td>
				<td><input type="submit" id="validar" class="btn btn-default"
					value="Salvar" onClick="submit()" class="save" /></td>
			</tr>
		</table>
	</form>

	<div style="clear: both;"></div>

</div>




<%-- Script que retira a m�scara dos dados antes de enviar --%>
<%-- Requer jquery.mask(.min).js --%>
<script type="text/javascript">
		$("#formCadastroPaciente").submit(function() {
			  $("#rg").unmask();
			  $("#cpf").unmask();
			  $("#telefone").unmask();
			})	
		</script>



</body>

</html>












<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="includes/menu.jsp" %>

 <div class="container">
  <h3>Lista de Especialidade</h3> 
</div>
	<div class="container"> 		
			<table class="table">			
				<tr>
					<th>C�digo</th>
					<th>Nome</th>
					<th>A��o</th>
				</tr>				
				<c:forEach var="tempEspecialidade" items="${ESPECIALIDADE_LIST}">					
					<c:url var="tempLink" value="EspecialidadeControllerServlet" >
						<c:param name="command" value="LOAD" />
						<c:param name="especialidadeId" value="${tempEspecialidade.codEspecialidade}" />
					</c:url>
					<c:url var="deleteLink" value="EspecialidadeControllerServlet">
						<c:param name="command" value="DELETE" />
						<c:param name="especialidadeId" value="${tempEspecialidade.codEspecialidade}" />
					</c:url>																		
					<tr>
						<td> ${tempEspecialidade.codEspecialidade} </td>
						<td> ${tempEspecialidade.nomeEspecialidade} </td>
						<td> 
							<a href="${tempLink}" class="btn btn-warning">Atualizar</a> 							   
							<a href="${deleteLink}"
							class="btn btn-danger" onclick="if (!(confirm('Tem certeza que deseja excluir registro?'))) return false">
							Apagar</a>	
						</td>
					</tr>				
				</c:forEach>				
			</table>		
		</div>	
	</div>
</body>
</html>









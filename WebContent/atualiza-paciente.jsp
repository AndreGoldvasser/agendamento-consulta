<%@include file="includes/menu.jsp" %>

 <div class="container">
  <h3>Atualizar Paciente</h3> 
</div> 	
	<div class="container">	
		<form action="PacienteControllerServlet" method="GET">
			<input type="hidden" name="command" value="UPDATE" />
			<input type="hidden" name="pacienteId" value="${PACIENTE.codPaciente}" />	
			<table class="table">
				<tbody>
					<tr>
						<td><label>Nome:</label></td>
						<td><input type="text" name="nomePaciente" maxlength="50" 
								   value="${PACIENTE.nomePaciente}" /></td>
					</tr>				
					
						<tr>
						<td><label>RG:</label></td>
						<td><input type="text" name="rgPaciente" maxlength="11"
								   value="${PACIENTE.rgPaciente}" /></td>
					</tr>	
					
						<tr>
						<td><label>CPF:</label></td>
						<td><input type="text" name="cpfPaciente" maxlength="11"
								   value="${PACIENTE.cpfPaciente}" /></td>
					</tr>	
					
						<tr>
						<td><label>Endere�o:</label></td>
						<td><input type="text" name="enderecoPaciente" maxlength="50"
								   value="${PACIENTE.enderecoPaciente}" /></td>
					</tr>	
					
					<tr>
						<td><label>Cidade:</label></td>
						<td>
							<select name="cidadePaciente">
    							<c:forEach items="${CIDADES_FORM}" var="tempCid">
        							<option value="${tempCid.codCidade}"><c:out value="${tempCid.nomeCidade}" /></option>
    							</c:forEach>
							</select>
						</td>
					</tr>
					
						<tr>
						<td><label>Bairro:</label></td>
						<td><input type="text" name="bairroPaciente" maxlength="20"
								   value="${PACIENTE.bairroPaciente}" /></td>
					</tr>	
					
						<tr>
						<td><label>Estado:</label></td>
						<td><input type="text" name="estadoPaciente" maxlength="2"
								   value="${PACIENTE.estadoPaciente}" /></td>
					</tr>	
					
					<tr>
						<td><label>Telefone:</label></td>
						<td><input type="text" name="telefonePaciente" maxlength="20"
								   value="${PACIENTE.telefonePaciente}" /></td>
					</tr>
					
					<tr>
						<td><label>E-mail:</label></td>
						<td><input type="text" name="emailPaciente" maxlength="50"
								   value="${PACIENTE.emailPaciente}" /></td>
					</tr>
					
					<tr>
						<td><label>Nascimento:</label></td>
						<td><input type="text" name="datanascPaciente" 
								   value="${PACIENTE.datanascPaciente}" /></td>
					</tr>
					
					<tr>
						<td><label>Sexo:</label></td>
						<td><input type="text" name="sexoPaciente" 
								   value="${PACIENTE.sexoPaciente}" /></td>
					</tr>
					
					<tr>
						<td><label>Cartao Sus:</label></td>
						<td><input type="text" name="cartaoPaciente" 
								   value="${PACIENTE.cartaoPaciente}" /></td>
					</tr>
					
					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Salvar" class="save" /></td>
					</tr>
					
				</tbody>
			</table>
		</form>
		
		<div style="clear: both;"></div>
		
		<p>
			<a href="PacienteControllerServlet">Voltar � Lista</a>
		</p>
	</div>
</body>

</html>












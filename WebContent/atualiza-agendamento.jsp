<%@include file="includes/menu.jsp" %>

 <div class="container">
  <h3>Atualizar Agendamento</h3> 
</div> 	
	<div class="container">		
		<form action="AgendamentoControllerServlet" method="POST">		
			<input type="hidden" name="command" value="UPDATE" />
			<input type="hidden" name="AgendamentoId" value="${AGENDAMENTO.codAgendamento}" />			
			<table class="table">
				<tbody>					
					<tr>
						<td><label>Data:</label></td>
						<td><input type="text" name="dtAgendamento" maxlength="50"
								   value="${AGENDAMENTO.dtAgendamento}" /></td>
					</tr>									
					<tr>
						<td><label>Hora:</label></td>
						<td><input type="text" name="horaAgendamento" maxlength="30"
								   value="${AGENDAMENTO.horaAgendamento}" /></td>
					</tr>						
					<tr>
						<td><label>Paciente:</label></td>
						<td><input type="text" name="pacienteAgendamento" maxlength="20"
								   value="${AGENDAMENTO.pacienteAgendamento}" /></td>
					</tr>				
					<tr>
						<td><label>Medico:</label></td>
						<td><input type="text" name="medicoAgendamento" maxlength="11" 
								   value="${AGENDAMENTO.medicoAgendamento}" /></td>
					</tr>						
					<tr>
						<td><label>Especialidade:</label></td>
						<td><input type="text" name="especialidadeAgendamento" maxlength="11" 
								   value="${AGENDAMENTO.especialidadeAgendamento}" /></td>
					</tr>	
				<tr>
					<td><label>Status :</label></td>
						<td><select class="status" name="statusAgendamento">
 							<option selected="selected" value="Aberto">Aberto</option>
							<option value="Aberto">Em andamento</option>
							<option value="Finalizado">Finalizado</option> 
							<option value="Cancelado">Cancelado</option>   
				 		</select></td>
		  	   </tr>										
				<tr>
					<td><label></label></td>
						<td><input type="submit" value="Salvar" class="save" /></td>
					</tr>					
				</tbody>
			</table>
		</form>		
		<div style="clear: both;"></div>		
		<p>
			<a href="AgendamentoControllerServlet">Voltar � Lista</a>
		</p>
	</div>
</body>
</html>












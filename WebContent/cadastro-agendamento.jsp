
<%@page import="java.sql.*"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
<%@include file="includes/menu.jsp" %>
  
  <div class="container">
 
		<h3>Cadastro de Agendamento</h3>		
		<form action="AgendamentoControllerServlet" method="GET" id="form">		
			<input type="hidden" name="command" value="ADD" />			
			 <table class="table">
					
					<tr>
						<td><label>Data:</label></td>
						<td><input type="date" name="dtAgendamento" placeholder="00/00/0000" maxlength="50" required/></td>
					</tr>
					
					<tr>
						<td><label>Hora:</label></td>
						<td><input type="text" name="horaAgendamento" placeholder="00:00" maxlength="30" required/></td>
					</tr>

				  <tr>	
					<td><label>Paciente:</label></td>
						<td><select class="paciente" name="pacienteAgendamento">
 								<option selected="selected">-- Paciente --</option> 
					    <%
					    try
					    {
					        Class.forName("com.mysql.jdbc.Driver"); 
					        Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/sus_agendamento","root","jeje2021"); //create connection
					                
					        PreparedStatement pstmt=null;
					                
					        pstmt=con.prepareStatement("select * from paciente");
					        ResultSet rs=pstmt.executeQuery(); 
					                
					        while(rs.next())
					        {
					        	%>
					            	<option value="<%=rs.getInt("cd_Paciente")%>">
                						 <%=rs.getString("nm_Paciente")%>
            						</option>
        						<%
        				}
           
        				con.close(); 
				    	}
				   		 catch(Exception e)
				    	{
				       		out.println(e);
				    	}
				    %>
				 		</select>
				 	</td>
				 </tr>	
			<tr>			 		 	
				<td><label>Especialidade:</label></td>
					<td><select class="especialidade" name="especialidadeAgendamento" onchange="esp_change()">
 						<option selected="selected">--Selecione a Especialidade--</option>
 
					    <%
					    try
					    {
					        Class.forName("com.mysql.jdbc.Driver"); 
					        Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/sus_agendamento","root","jeje2021"); //create connection
					                
					        PreparedStatement pstmt=null;
					                
					        pstmt=con.prepareStatement("select * from especialidade");
					        ResultSet rs=pstmt.executeQuery(); 
					                
					        while(rs.next())
					        {
					        	%>
					            	<option value="<%=rs.getInt("cd_Especialidade")%>">
                						 <%=rs.getString("nm_Especialidade")%>
            						</option>
        						<%
        				}
           
        				con.close(); 
				    	}
				   		 catch(Exception e)
				    	{
				       		out.println(e);
				    	}
				    %>
				 		</select>
				 	</td>
				 </tr>	
			<tr>	
				<td><label>M�dico:</label></td> 
			 		<td><select class="medico" name="medicoAgendamento">
			 			<option selected="selected">--Selecione o  M�dico--</option>
			 		</select>
				</td>
			</tr>
			<tr>
				<td><label></label></td>
				<td><input type="submit" value="Salvar" class="save"/></td>
			</tr>					
			</table>
		</form>
		
		<div style="clear: both;"></div>
		
	</div>
</body>

</html>












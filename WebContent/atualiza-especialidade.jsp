<%@include file="includes/menu.jsp" %>

 <div class="container">
  <h3>Atualizar Especialidade</h3> 
</div>
	<div class="container"> 
		<form action="EspecialidadeControllerServlet" method="POST">		
			<input type="hidden" name="command" value="UPDATE" />
			<input type="hidden" name="especialidadeId" value="${ESPECIALIDADE.codEspecialidade}" />			
			<table class="table">
				<tbody>				
					<tr>
						<td><label>Nome:</label></td>
						<td><input type="text" name="nomeEspecialidade" 
								   value="${ESPECIALIDADE.nomeEspecialidade}"
								   maxlength="30" /></td>
					</tr>									
					<tr>
						<td><label></label></td>
						<td><input class="btn btn-default" type="submit" value="Salvar" class="save" /></td>
					</tr>					
				</tbody>
			</table>
		</form>		
		<div style="clear: both;"></div>	
		<p>
			<a href="EspecialidadeControllerServlet">Voltar � Lista</a>
		</p>
	</div>
</body>

</html>












 
<%@page import="com.sus.web.jdbc.EspecialidadeControllerServlet"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
<%@include file="includes/menu.jsp" %>
	 
  <div class="container">
     <h3>Cadastro de Especialidades</h3>   
  </div>
	
	<div class="container"> 	
		<form action="EspecialidadeControllerServlet" method="POST">		
			<input type="hidden" name="command" value="ADD" />			
			<table class="table">
				<tbody>
					<tr>
						<td><label>Especialidade:</label></td>
						<td><input type="text" name="nomeEspecialidade" placeholder="Especialidade" maxlength="30" /></td>
					</tr>						
					<tr>					
						<td><label></label></td>
						<td><input type="submit" class="btn btn-default" value="Salvar" class="save" /></td>
					</tr>					
				</tbody>
			</table>
						
			
		</form>
		
		<div style="clear: both;"></div>
		
		<p>
			<a href="EspecialidadeControllerServlet">Voltar � Lista</a>
		</p>
	</div>
</body>

</html>












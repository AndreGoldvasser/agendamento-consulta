<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="includes/menu.jsp"%>

<div class="container">
	<h3>Cadastro de M�dico</h3>

	<%-- Script que formata os campos corretamente --%>
	<%-- Requer jquery.mask(.min).js --%>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#rg").mask("999.999.999-W", {
				translation : {
					'W' : {
						pattern : /[X0-9]/
					}
				},
				reverse : true
			})

			$("#cpf").mask("000.000.000-00")
			$("#telefone").mask("(00) 0000-00009")
			$("#telefone").blur(function(event) {
				if ($(this).val().length == 15) {
					$("#telefone").mask("(00) 00000-0009")
				} else {
					$("#telefone").mask("(00) 0000-0009")
				}
			})

			$("#crm").mask("0000999999-SS")

		})
	</script>



	<form action="MedicoControllerServlet" method="GET">

		<input type="hidden" name="command" value="ADD" />

		<table class="table">
			<tr>
				<td><label>Nome:</label></td>
				<td><input type="text" name="nomeMedico" placeholder="Jane Doe"
					maxlength="50" /></td>
			</tr>

			<tr>
				<td><label>CRM:</label></td>
				<td><input type="text" id="crm" name="crmMedico"
					placeholder="98128734" maxlength="20" /></td>
			</tr>

			<tr>
				<td><label>Especialidade:</label></td>
				<td><select name="especialidadeMedico">
						<c:forEach items="${ESPECIALIDADES_FORM}" var="tempEsp">
							<option value="${tempEsp.codEspecialidade}"><c:out
									value="${tempEsp.nomeEspecialidade}" /></option>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td><label>RG:</label></td>
				<td><input type="text" id="rg" name="rgMedico"
					placeholder="12345678900" maxlength="11" /></td>
			</tr>

			<tr>
				<td><label>CPF:</label></td>
				<td><input type="text" id="cpf" name="cpfMedico"
					placeholder="00987654321" maxlength="11" /></td>
			</tr>
			<tr>
				<td><label>Endere�o:</label></td>
				<td><input type="text" name="enderecoMedico"
					placeholder="Rua ZYX, 999" maxlength="50" /></td>
			</tr>

			<tr>
				<td><label>Cidade:</label></td>
				<td><select name="cidadeMedico">
						<option value="">---SELECIONE---</option>
						<c:forEach items="${CIDADES_FORM}" var="tempCid">
							<option value="${tempCid.codCidade}"><c:out
									value="${tempCid.nomeCidade}" /></option>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td><label>Bairro:</label></td>
				<td><input type="text" name="bairroMedico" maxlength="20"
					placeholder="Meu Bairro" /></td>
			</tr>
			<tr>
				<td><label>Estado:</label></td>
				<td><input type="text" name="estadoMedico" maxlength="2"
					placeholder="SP" /></td>
			</tr>
			<tr>
				<td><label>Telefone:</label></td>
				<td><input type="text" id="telefone" name="telefoneMedico"
					placeholder="555666000" maxlength="20" /></td>
			</tr>
			<tr>
				<td><label>E-Mail:</label></td>
				<td><input type="email" name="emailMedico"
					placeholder="mymail@mymail.com" maxlength="50" /></td>
			</tr>

			<tr>
				<td><label>Nascimento:</label></td>
				<td><input type="date" name="datanascMedico"
					placeholder="00/00/0000" /></td>
			</tr>

			<tr>
				<td><label></label></td>
				<td><input type="submit" class="btn-btn-default" value="Salvar"
					class="save" onClick="submit()" /></td>
			</tr>
		</table>
	</form>

	<div style="clear: both;"></div>
</div>




<%-- Script que retira a m�scara dos dados antes de enviar --%>
<%-- Requer jquery.mask(.min).js --%>
<script type="text/javascript">
	$("#formCadastroPaciente").submit(function() {
		$("#rg").unmask();
		$("#cpf").unmask();
		$("#telefone").unmask();
	})
</script>

</body>

</html>












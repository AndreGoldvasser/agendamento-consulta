<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="includes/menu.jsp" %>

 <div class="container">
  <h3>Agendamentos</h3> 
</div>
	<div class="container">			
			<table class="table">					
				<div class="row">
			        <div class="col-sm-6 col-sm-offset-3">
			            <div id="imaginary_container"> 
			                <div class="input-group stylish-input-group">
			                    <input id="paciente" type="text" class="form-control"  placeholder="Pesquisar paciente" >
			                    <span class="input-group-addon">
			                    <button id="paciente" style="border:none; background-color: transparent;">			                       
			                            <span class="glyphicon glyphicon-search"></span>
			                    </button>        			                       
			                    </span>
			                </div>
			            </div>
			        </div>
				</div>		
			<br/><br/><br/>
				<tr>
					<th>C�digo</th>
					<th>Data</th>
					<th>Hora</th>
					<th>Paciente</th>
					<th>M�dico</th>
					<th>Especialidade</th>				
					<th>Status</th>
					<th>A��es</th>					
				</tr>				
				<c:forEach var="tempAgendamento" items="${AGENDAMENTO_LIST}">					
					<c:url var="tempLink" value="AgendamentoControllerServlet">
						<c:param name="command" value="LOAD" />
						<c:param name="AgendamentoId" value="${tempAgendamento.codAgendamento}" />
					</c:url>
					<c:url var="deleteLink" value="AgendamentoControllerServlet">
						<c:param name="command" value="DELETE" />
						<c:param name="AgendamentoId" value="${tempAgendamento.codAgendamento}" />
					</c:url>																											
					<tr>
						<td> ${tempAgendamento.codAgendamento} </td>
						<td> ${tempAgendamento.dtAgendamento} </td>
						<td> ${tempAgendamento.horaAgendamento} </td>
						<td> ${tempAgendamento.pacienteAgendamento}</td>
						<td> ${tempAgendamento.medicoAgendamento} </td>
						<td> ${tempAgendamento.especialidadeAgendamento} </td>		
						<td> ${tempAgendamento.statusAgendamento} </td>				
						<td> 
							<a class="btn btn-warning" href="${tempLink}">Atualizar</a>						 
							<a class="btn btn-danger" href="${deleteLink}"
							onclick="if (!(confirm('Tem certeza que deseja excluir registro?'))) return false">
							Apagar</a>	
						</td>
					</tr>				
				</c:forEach>				
			</table>		
		</div>	
	</div>
</body>
</html>









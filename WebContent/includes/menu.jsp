<%
//Verifica se está logado 
String nm_Usuario = (String) session.getAttribute("nm_Usuario");
if(nm_Usuario!=null)
{
//usuário válido
}
else
{
//retorna caso o usuário está inválido
	
response.sendRedirect("http://localhost:8080/agendamento-java/acesso-negado.jsp");
}

%>
<!DOCTYPE html>
<head>
  <title>Agendamento </title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/style.css"/>
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>

  <script src="js/jquery.autocomplete.js"></script>	 
  <script src="js/jquery.mask.min.js"></script>
  <script src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
function esp_change()
{
    var especialidade = $(".especialidade").val();
 
    $.ajax({
        type: "POST",
        url: "medicoAgendamento.jsp",
        data: "cd_Especialidade="+especialidade,
        cache: false,
        success: function(response)
        {
            $(".medico").html(response);
        }
    });
}

$(document).ready(function(){
            $("#nome").autocomplete("autocomplete-paciente.jsp", {
                        scroll: true,
                        scrollHeight: 10,
                        maxHeight: 10,
                        width: 280,
                        selectFirst: true
                    });
});

</script>


</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="AgendamentoControllerServlet?command=CADASTRO" >Novo Agendamento</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="AgendamentoControllerServlet">Inicio</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Listas <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <!-- <li><a href="AgendamentoControllerServlet">Agendamento</a></li> -->
          <li><a href="EspecialidadeControllerServlet">Especialidade</a></li>
          <li><a href="MedicoControllerServlet">Medico</a></li>
          <li><a href="PacienteControllerServlet">Paciente</a></li>
        </ul>
      </li>
       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Cadastros <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="EspecialidadeControllerServlet?command=CADASTRO">Especialidade</a></li>
          <li><a href="MedicoControllerServlet?command=CADASTRO">Medico</a></li>
          <li><a href="PacienteControllerServlet?command=CADASTRO">Paciente</a></li>
        </ul>
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="logout.jsp"><span class="glyphicon glyphicon-user"></span> Sair</a></li>
    </ul>
  </div>
</nav>
 

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agendamento de Consulta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <label>Paciente:</label>
			<input class="form-control" type="text" name="nomePaciente" placeholder="João Silva" maxlength="50"/>
      <label>Médico:</label>
			<input type="text" class="form-control" name="nomeMedico" placeholder="Dr. Fabiano Doe" maxlength="50" />  
      <label>Especialidade:</label>
			<input class="form-control" type="text" name="nomeEspecialidade" placeholder="Especialidade" maxlength="30" />
	  <label>Data:</label>
			<input class="form-control" type="date" name="datanascMedico" placeholder="00/00/0000"/> <br/>
	 
	  <label>Horário</label>
	  <input id="time" type="time">
      </div>
      <div class="modal-footer">        
        <button type="button" class="btn btn-primary">Salvar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
  
</body>
</html>
